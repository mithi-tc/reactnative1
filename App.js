import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  Alert 
} from 'react-native';



const App: () => React$Node = () => {

const [email,setEmail]=useState(null);

validate = () => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(email) === false) {
    Alert.alert(
      'Validation Results',
      email+' is invalid', 
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }
  else {

    Alert.alert(
      'Validation Results',
      email + ' is valid', 
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
   
  }
}  

  return (
    <>
      <View style={{backgroundColor:'white'}} >
        <Text style={{fontSize:20,fontWeight:'bold',padding:10}}>React Native Training</Text>

      </View>
       
          <View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'grey'}}>
            <TextInput style={{backgroundColor:'white',width:350,marginBottom:20}}
            placeholder='Email'
            onChangeText={(email)=>{setEmail(email)}}
            value={email}
             />
            <View style={{width:100}}>
              <Button  
              title='Test'
              onPress={ this.validate.bind(this)} />
            </View>
          
          </View>
      
      
    </>
  );
};



export default App;
